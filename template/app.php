<?php

namespace Summo\Picture;

class SummoPicture
{

    public function boot()
    {
        $this->scripts();
        $this->styles();
    }

    private function scripts()
    {
        if(is_admin())return;
        wp_enqueue_script('summo-jquery',get_template_directory_uri() . '/assets/js/jquery.min.js','',false,true);
        wp_enqueue_script('summo-tether', 'https://www.atlasestateagents.co.uk/javascript/tether.min.js', 'summo-jquery', false, true);
        wp_enqueue_script('summo-bootstrap', get_template_directory_uri() . '/assets/js/bootstrap.min.js', 'summo-jquery', false, true);
        wp_enqueue_script('summo-hero-slider', get_template_directory_uri() . '/assets/js/hero-slider-main.js', 'summo-jquery', false, true);
        wp_enqueue_script('summo-magnify-popup', get_template_directory_uri() . '/assets/js/jquery.magnific-popup.min.js', 'summo-jquery', false, true);
    }

    private function styles()
    {
        if(is_admin())return;
        wp_enqueue_style('summo-font', 'https://fonts.googleapis.com/css?family=Open+Sans:300,400');
        wp_enqueue_style('summo-bootstrap', get_template_directory_uri() . '/assets/css/bootstrap.min.css');
        wp_enqueue_style('summo-hero-slider', get_template_directory_uri() . '/assets/css/hero-slider-style.css');
        wp_enqueue_style('summo-magnific', get_template_directory_uri() . '/assets/css/magnific-popup.css');
        wp_enqueue_style('summo-toolplace', get_template_directory_uri() . '/assets/css/tooplate-style.css');
    }
}