<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package _s
 */

get_header(); ?>

    <ul class="cd-hero-slider">
        <?php foreach (wp_get_nav_menu_items('menu-1') as $key => $menu) :
            $post = get_post($menu->object_id);
            if (has_shortcode($post->post_content, 'gallery')) :
               //TODO
            else:
            endif
            ?>
            <!-- Page 1 Gallery One -->
            <li class="<?php echo !$key ? 'selected' : '' ?>">
                <div class="cd-full-width">
                    <div class="container-fluid js-tm-page-content" data-page-no="1" data-page-type="gallery">
                        <div class="tm-img-gallery-container">
                            <div class="tm-img-gallery gallery-one">
                                <!-- Gallery One pop up connected with JS code below -->
                                <div class="tm-img-gallery-info-container">
                                    <h2 class="tm-text-title tm-gallery-title tm-white"><span class="tm-white">Multi Color Image Gallery</span>
                                    </h2>
                                    <p class="tm-text">This responsive HTML template includes three gallery pages. Multi
                                        color is designed by Tooplate. You may use this layout for your website.
                                    </p>
                                </div>
                                <div class="grid-item">
                                    <figure class="effect-bubba">
                                        <img src="img/tm-img-01-tn.jpg" alt="Image" class="img-fluid tm-img">
                                        <figcaption>
                                            <h2 class="tm-figure-title">Image <span>One</span></h2>
                                            <p class="tm-figure-description">Lorem ipsum dolor sit amet, consectetur
                                                adipiscing elit.</p>
                                            <a href="img/tm-img-01.jpg">View more</a>
                                        </figcaption>
                                    </figure>
                                </div>
                                <div class="grid-item">
                                    <figure class="effect-bubba">
                                        <img src="img/tm-img-02-tn.jpg" alt="Image" class="img-fluid tm-img">
                                        <figcaption>
                                            <h2 class="tm-figure-title">Image <span>Two</span></h2>
                                            <p class="tm-figure-description">Maecenas purus sem, lobortis id odio in
                                                sapien.</p>
                                            <a href="img/tm-img-02.jpg">View more</a>
                                        </figcaption>
                                    </figure>
                                </div>
                                <div class="grid-item">
                                    <figure class="effect-bubba">
                                        <img src="img/tm-img-03-tn.jpg" alt="Image" class="img-fluid tm-img">
                                        <figcaption>
                                            <h2 class="tm-figure-title">Image <span>Three</span></h2>
                                            <p class="tm-figure-description">Lorem ipsum dolor sit amet, consectetur
                                                adipiscing elit.</p>
                                            <a href="img/tm-img-03.jpg">View more</a>
                                        </figcaption>
                                    </figure>
                                </div>
                                <div class="grid-item">
                                    <figure class="effect-bubba">
                                        <img src="img/tm-img-04-tn.jpg" alt="Image" class="img-fluid tm-img">
                                        <figcaption>
                                            <h2 class="tm-figure-title">Image <span>Four</span></h2>
                                            <p class="tm-figure-description">Maecenas purus sem, lobortis id odio in
                                                sapien.</p>
                                            <a href="img/tm-img-04.jpg">View more</a>
                                        </figcaption>
                                    </figure>
                                </div>
                                <div class="grid-item">
                                    <figure class="effect-bubba">
                                        <img src="img/tm-img-05-tn.jpg" alt="Image" class="img-fluid tm-img">
                                        <figcaption>
                                            <h2 class="tm-figure-title">Image <span>Five</span></h2>
                                            <p class="tm-figure-description">Lorem ipsum dolor sit amet, consectetur
                                                adipiscing elit.</p>
                                            <a href="img/tm-img-05.jpg">View more</a>
                                        </figcaption>
                                    </figure>
                                </div>
                                <div class="grid-item">
                                    <figure class="effect-bubba">
                                        <img src="img/tm-img-06-tn.jpg" alt="Image" class="img-fluid tm-img">
                                        <figcaption>
                                            <h2 class="tm-figure-title">Image <span>Six</span></h2>
                                            <p class="tm-figure-description">Maecenas purus sem, lobortis id odio in
                                                sapien.</p>
                                            <a href="img/tm-img-06.jpg">View more</a>
                                        </figcaption>
                                    </figure>
                                </div>
                                <div class="grid-item">
                                    <figure class="effect-bubba">
                                        <img src="img/tm-img-07-tn.jpg" alt="Image" class="img-fluid tm-img">
                                        <figcaption>
                                            <h2 class="tm-figure-title">Image <span>Seven</span></h2>
                                            <p class="tm-figure-description">Lorem ipsum dolor sit amet, consectetur
                                                adipiscing elit.</p>
                                            <a href="img/tm-img-07.jpg">View more</a>
                                        </figcaption>
                                    </figure>
                                </div>
                                <div class="grid-item">
                                    <figure class="effect-bubba">
                                        <img src="img/tm-img-08-tn.jpg" alt="Image" class="img-fluid tm-img">
                                        <figcaption>
                                            <h2 class="tm-figure-title">Image <span>Eight</span></h2>
                                            <p class="tm-figure-description">Maecenas purus sem, lobortis id odio in
                                                sapien.</p>
                                            <a href="img/tm-img-08.jpg">View more</a>
                                        </figcaption>
                                    </figure>
                                </div>
                                <div class="grid-item">
                                    <figure class="effect-bubba">
                                        <img src="img/tm-img-09-tn.jpg" alt="Image" class="img-fluid tm-img">
                                        <figcaption>
                                            <h2 class="tm-figure-title">Image <span>Nine</span></h2>
                                            <p class="tm-figure-description">Lorem ipsum dolor sit amet, consectetur
                                                adipiscing elit.</p>
                                            <a href="img/tm-img-09.jpg">View more</a>
                                        </figcaption>
                                    </figure>
                                </div>
                                <div class="grid-item">
                                    <figure class="effect-bubba">
                                        <img src="img/tm-img-10-tn.jpg" alt="Image" class="img-fluid tm-img">
                                        <figcaption>
                                            <h2 class="tm-figure-title">Image <span>Ten</span></h2>
                                            <p class="tm-figure-description">Maecenas purus sem, lobortis id odio in
                                                sapien.</p>
                                            <a href="img/tm-img-10.jpg">View more</a>
                                        </figcaption>
                                    </figure>
                                </div>
                                <div class="grid-item">
                                    <figure class="effect-bubba">
                                        <img src="img/tm-img-11-tn.jpg" alt="Image" class="img-fluid tm-img">
                                        <figcaption>
                                            <h2 class="tm-figure-title">Image <span>Eleven</span></h2>
                                            <p class="tm-figure-description">Lorem ipsum dolor sit amet, consectetur
                                                adipiscing elit.</p>
                                            <a href="img/tm-img-11.jpg">View more</a>
                                        </figcaption>
                                    </figure>
                                </div>
                                <div class="grid-item">
                                    <figure class="effect-bubba">
                                        <img src="img/tm-img-12-tn.jpg" alt="Image" class="img-fluid tm-img">
                                        <figcaption>
                                            <h2 class="tm-figure-title">Image <span>Twelve</span></h2>
                                            <p class="tm-figure-description">Maecenas purus sem, lobortis id odio in
                                                sapien.</p>
                                            <a href="img/tm-img-12.jpg">View more</a>
                                        </figcaption>
                                    </figure>
                                </div>
                                <div class="grid-item">
                                    <figure class="effect-bubba">
                                        <img src="img/tm-img-13-tn.jpg" alt="Image" class="img-fluid tm-img">
                                        <figcaption>
                                            <h2 class="tm-figure-title">Image <span>Thirteen</span></h2>
                                            <p class="tm-figure-description">Lorem ipsum dolor sit amet, consectetur
                                                adipiscing elit.</p>
                                            <a href="img/tm-img-13.jpg">View more</a>
                                        </figcaption>
                                    </figure>
                                </div>
                                <div class="grid-item">
                                    <figure class="effect-bubba">
                                        <img src="img/tm-img-14-tn.jpg" alt="Image" class="img-fluid tm-img">
                                        <figcaption>
                                            <h2 class="tm-figure-title">Image <span>Fourteen</span></h2>
                                            <p class="tm-figure-description">Maecenas purus sem, lobortis id odio in
                                                sapien.</p>
                                            <a href="img/tm-img-14.jpg">View more</a>
                                        </figcaption>
                                    </figure>
                                </div>
                                <div class="grid-item">
                                    <figure class="effect-bubba">
                                        <img src="img/tm-img-15-tn.jpg" alt="Image" class="img-fluid tm-img">
                                        <figcaption>
                                            <h2 class="tm-figure-title">Image <span>Fifteen</span></h2>
                                            <p class="tm-figure-description">Lorem ipsum dolor sit amet, consectetur
                                                adipiscing elit.</p>
                                            <a href="img/tm-img-15.jpg">View more</a>
                                        </figcaption>
                                    </figure>
                                </div>
                                <div class="grid-item">
                                    <figure class="effect-bubba">
                                        <img src="img/tm-img-16-tn.jpg" alt="Image" class="img-fluid tm-img">
                                        <figcaption>
                                            <h2 class="tm-figure-title">Image <span>Sixteen</span></h2>
                                            <p class="tm-figure-description">Maecenas purus sem, lobortis id odio in
                                                sapien.</p>
                                            <a href="img/tm-img-16.jpg">View more</a>
                                        </figcaption>
                                    </figure>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </li>
        <?php endforeach; ?>

        <!-- Page 2 Gallery Two -->
        <li>
            <div class="cd-full-width">
                <div class="container-fluid js-tm-page-content" data-page-no="2" data-page-type="gallery">
                    <div class="tm-img-gallery-container">
                        <div class="tm-img-gallery gallery-two">
                            <!-- Gallery Two pop up connected with JS code below -->

                            <div class="tm-img-gallery-info-container">
                                <h2 class="tm-text-title tm-gallery-title"><span
                                            class="tm-white">Multi Two Gallery</span></h2>
                                <p class="tm-text"><span class="tm-white">Etiam gravida et elit vitae maximus. Pellentesque fringilla felis id feugiat consectetur. Sed quis commodo leo. Nunc aliquet auctor nunc, sit amet pharetra metus commodo ut.</span>
                                </p>
                            </div>
                            <div class="grid-item">
                                <figure class="effect-bubba">
                                    <img src="img/tm-img-12-tn.jpg" alt="Image" class="img-fluid tm-img">
                                    <figcaption>
                                        <h2 class="tm-figure-title">Picture <span>One</span></h2>
                                        <p class="tm-figure-description">Suspendisse id placerat risus. Mauris quis
                                            luctus risus.</p>
                                        <a href="img/tm-img-12.jpg">View more</a>
                                    </figcaption>
                                </figure>
                            </div>
                            <div class="grid-item">
                                <figure class="effect-bubba">
                                    <img src="img/tm-img-11-tn.jpg" alt="Image" class="img-fluid tm-img">
                                    <figcaption>
                                        <h2 class="tm-figure-title">Picture <span>Two</span></h2>
                                        <p class="tm-figure-description">Maecenas purus sem, lobortis id odio in
                                            sapien.</p>
                                        <a href="img/tm-img-11.jpg">View more</a>
                                    </figcaption>
                                </figure>
                            </div>
                            <div class="grid-item">
                                <figure class="effect-bubba">
                                    <img src="img/tm-img-10-tn.jpg" alt="Image" class="img-fluid tm-img">
                                    <figcaption>
                                        <h2 class="tm-figure-title">Picture <span>Three</span></h2>
                                        <p class="tm-figure-description">Suspendisse id placerat risus. Mauris quis
                                            luctus risus.</p>
                                        <a href="img/tm-img-10.jpg">View more</a>
                                    </figcaption>
                                </figure>
                            </div>
                            <div class="grid-item">
                                <figure class="effect-bubba">
                                    <img src="img/tm-img-09-tn.jpg" alt="Image" class="img-fluid tm-img">
                                    <figcaption>
                                        <h2 class="tm-figure-title">Picture <span>Four</span></h2>
                                        <p class="tm-figure-description">Maecenas purus sem, lobortis id odio in
                                            sapien.</p>
                                        <a href="img/tm-img-09.jpg">View more</a>
                                    </figcaption>
                                </figure>
                            </div>
                            <div class="grid-item">
                                <figure class="effect-bubba">
                                    <img src="img/tm-img-08-tn.jpg" alt="Image" class="img-fluid tm-img">
                                    <figcaption>
                                        <h2 class="tm-figure-title">Picture <span>Five</span></h2>
                                        <p class="tm-figure-description">Suspendisse id placerat risus. Mauris quis
                                            luctus risus.</p>
                                        <a href="img/tm-img-08.jpg">View more</a>
                                    </figcaption>
                                </figure>
                            </div>
                            <div class="grid-item">
                                <figure class="effect-bubba">
                                    <img src="img/tm-img-07-tn.jpg" alt="Image" class="img-fluid tm-img">
                                    <figcaption>
                                        <h2 class="tm-figure-title">Picture <span>Six</span></h2>
                                        <p class="tm-figure-description">Maecenas purus sem, lobortis id odio in
                                            sapien.</p>
                                        <a href="img/tm-img-07.jpg">View more</a>
                                    </figcaption>
                                </figure>
                            </div>
                            <div class="grid-item">
                                <figure class="effect-bubba">
                                    <img src="img/tm-img-06-tn.jpg" alt="Image" class="img-fluid tm-img">
                                    <figcaption>
                                        <h2 class="tm-figure-title">Picture <span>Seven</span></h2>
                                        <p class="tm-figure-description">Maecenas purus sem, lobortis id odio in
                                            sapien.</p>
                                        <a href="img/tm-img-06.jpg">View more</a>
                                    </figcaption>
                                </figure>
                            </div>
                            <div class="grid-item">
                                <figure class="effect-bubba">
                                    <img src="img/tm-img-05-tn.jpg" alt="Image" class="img-fluid tm-img">
                                    <figcaption>
                                        <h2 class="tm-figure-title">Picture <span>Eight</span></h2>
                                        <p class="tm-figure-description">Maecenas purus sem, lobortis id odio in
                                            sapien.</p>
                                        <a href="img/tm-img-05.jpg">View more</a>
                                    </figcaption>
                                </figure>
                            </div>
                            <div class="grid-item">
                                <figure class="effect-bubba">
                                    <img src="img/tm-img-04-tn.jpg" alt="Image" class="img-fluid tm-img">
                                    <figcaption>
                                        <h2 class="tm-figure-title">Picture <span>Nine</span></h2>
                                        <p class="tm-figure-description">Maecenas purus sem, lobortis id odio in
                                            sapien.</p>
                                        <a href="img/tm-img-04.jpg">View more</a>
                                    </figcaption>
                                </figure>
                            </div>
                            <div class="grid-item">
                                <figure class="effect-bubba">
                                    <img src="img/tm-img-03-tn.jpg" alt="Image" class="img-fluid tm-img">
                                    <figcaption>
                                        <h2 class="tm-figure-title">Picture <span>Ten</span></h2>
                                        <p class="tm-figure-description">Maecenas purus sem, lobortis id odio in
                                            sapien.</p>
                                        <a href="img/tm-img-03.jpg">View more</a>
                                    </figcaption>
                                </figure>
                            </div>
                            <div class="grid-item">
                                <figure class="effect-bubba">
                                    <img src="img/tm-img-02-tn.jpg" alt="Image" class="img-fluid tm-img">
                                    <figcaption>
                                        <h2 class="tm-figure-title">Picture <span>Eleven</span></h2>
                                        <p class="tm-figure-description">Maecenas purus sem, lobortis id odio in
                                            sapien.</p>
                                        <a href="img/tm-img-02.jpg">View more</a>
                                    </figcaption>
                                </figure>
                            </div>
                            <div class="grid-item">
                                <figure class="effect-bubba">
                                    <img src="img/tm-img-01-tn.jpg" alt="Image" class="img-fluid tm-img">
                                    <figcaption>
                                        <h2 class="tm-figure-title">Picture <span>Twelve</span></h2>
                                        <p class="tm-figure-description">Maecenas purus sem, lobortis id odio in
                                            sapien.</p>
                                        <a href="img/tm-img-01.jpg">View more</a>
                                    </figcaption>
                                </figure>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </li>

        <!-- Page 3 Gallery Three -->
        <li>
            <div class="cd-full-width">
                <div class="container-fluid js-tm-page-content" data-page-no="3" data-page-type="gallery">
                    <div class="tm-img-gallery-container">
                        <div class="tm-img-gallery gallery-three">
                            <!-- Gallery Two pop up connected with JS code below -->

                            <div class="tm-img-gallery-info-container">
                                <h2 class="tm-text-title tm-gallery-title"><span
                                            class="tm-white">Third Multi Gallery</span></h2>
                                <p class="tm-text"><span class="tm-white">Donec dapibus dui sed nisi fermentum, a sollicitudin lorem fringilla. Integer nec pharetra turpis, eu sagittis ipsum. Cras dignissim lacus dolor.</span>
                                </p>
                            </div>
                            <div class="grid-item">
                                <figure class="effect-bubba">
                                    <img src="img/tm-img-01-tn.jpg" alt="Image" class="img-fluid tm-img">
                                    <figcaption>
                                        <h2 class="tm-figure-title">Picture <span>One</span></h2>
                                        <p class="tm-figure-description">Suspendisse id placerat risus. Mauris quis
                                            luctus risus.</p>
                                        <a href="img/tm-img-01.jpg">View more</a>
                                    </figcaption>
                                </figure>
                            </div>
                            <div class="grid-item">
                                <figure class="effect-bubba">
                                    <img src="img/tm-img-06-tn.jpg" alt="Image" class="img-fluid tm-img">
                                    <figcaption>
                                        <h2 class="tm-figure-title">Picture <span>Two</span></h2>
                                        <p class="tm-figure-description">Maecenas purus sem, lobortis id odio in
                                            sapien.</p>
                                        <a href="img/tm-img-06.jpg">View more</a>
                                    </figcaption>
                                </figure>
                            </div>
                            <div class="grid-item">
                                <figure class="effect-bubba">
                                    <img src="img/tm-img-13-tn.jpg" alt="Image" class="img-fluid tm-img">
                                    <figcaption>
                                        <h2 class="tm-figure-title">Picture <span>Three</span></h2>
                                        <p class="tm-figure-description">Suspendisse id placerat risus. Mauris quis
                                            luctus risus.</p>
                                        <a href="img/tm-img-13.jpg">View more</a>
                                    </figcaption>
                                </figure>
                            </div>
                            <div class="grid-item">
                                <figure class="effect-bubba">
                                    <img src="img/tm-img-12-tn.jpg" alt="Image" class="img-fluid tm-img">
                                    <figcaption>
                                        <h2 class="tm-figure-title">Picture <span>Four</span></h2>
                                        <p class="tm-figure-description">Maecenas purus sem, lobortis id odio in
                                            sapien.</p>
                                        <a href="img/tm-img-12.jpg">View more</a>
                                    </figcaption>
                                </figure>
                            </div>
                            <div class="grid-item">
                                <figure class="effect-bubba">
                                    <img src="img/tm-img-05-tn.jpg" alt="Image" class="img-fluid tm-img">
                                    <figcaption>
                                        <h2 class="tm-figure-title">Picture <span>Five</span></h2>
                                        <p class="tm-figure-description">Suspendisse id placerat risus. Mauris quis
                                            luctus risus.</p>
                                        <a href="img/tm-img-05.jpg">View more</a>
                                    </figcaption>
                                </figure>
                            </div>
                            <div class="grid-item">
                                <figure class="effect-bubba">
                                    <img src="img/tm-img-09-tn.jpg" alt="Image" class="img-fluid tm-img">
                                    <figcaption>
                                        <h2 class="tm-figure-title">Picture <span>Six</span></h2>
                                        <p class="tm-figure-description">Maecenas purus sem, lobortis id odio in
                                            sapien.</p>
                                        <a href="img/tm-img-09.jpg">View more</a>
                                    </figcaption>
                                </figure>
                            </div>
                            <div class="grid-item">
                                <figure class="effect-bubba">
                                    <img src="img/tm-img-11-tn.jpg" alt="Image" class="img-fluid tm-img">
                                    <figcaption>
                                        <h2 class="tm-figure-title">Picture <span>Seven</span></h2>
                                        <p class="tm-figure-description">Suspendisse id placerat risus. Mauris quis
                                            luctus risus.</p>
                                        <a href="img/tm-img-11.jpg">View more</a>
                                    </figcaption>
                                </figure>
                            </div>
                            <div class="grid-item">
                                <figure class="effect-bubba">
                                    <img src="img/tm-img-14-tn.jpg" alt="Image" class="img-fluid tm-img">
                                    <figcaption>
                                        <h2 class="tm-figure-title">Picture <span>Eight</span></h2>
                                        <p class="tm-figure-description">Maecenas purus sem, lobortis id odio in
                                            sapien.</p>
                                        <a href="img/tm-img-14.jpg">View more</a>
                                    </figcaption>
                                </figure>
                            </div>
                        </div>
                    </div> <!-- .tm-img-gallery-container -->
                </div>
            </div>
        </li>

        <!-- Page 4 About -->
        <li>
            <div class="cd-full-width">
                <div class="container-fluid js-tm-page-content tm-page-width tm-pad-b" data-page-no="4">
                    <div class="row tm-white-box-margin-b">
                        <div class="col-xs-12">
                            <div class="tm-flex">
                                <div class="tm-bg-white-translucent text-xs-left tm-textbox tm-textbox-padding">
                                    <h2 class="tm-text-title">About our team</h2>
                                    <p class="tm-text">Quisque efficitur dui id turpis cursus, quis faucibus nulla
                                        malesuada. Nulla consectetur eget quam id pulvinar. Nulla facilisi. Curabitur
                                        rhoncus lacinia tincidunt. Etiam velit dui, rutrum vel finibus ac, commodo at
                                        mauris. Donec vitae diam ac tellus consectetur interdum eu non odio.</p>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row tm-white-box-margin-b">
                        <div class="col-xs-12">
                            <div class="tm-flex">
                                <div class="tm-bg-white-translucent text-xs-left tm-textbox tm-2-col-textbox-2 tm-textbox-padding">
                                    <h2 class="tm-text-title">Nulla vitae magna</h2>
                                    <p class="tm-text">Aliquam porttitor tortor at nisi fermentum, ac porta arcu
                                        vulputate. Nunc lobortis ipsum sapien, non ultrices odio tempus varius. In
                                        posuere dolor non sagittis ultrices.</p>
                                </div>
                                <div class="tm-bg-white-translucent text-xs-left tm-textbox tm-2-col-textbox-2 tm-textbox-padding">
                                    <h2 class="tm-text-title">Vivamus aliquam turpis</h2>
                                    <p class="tm-text">Integer quis leo pretium, cursus nisl non, placerat magna. Sed
                                        efficitur massa id magna eleifend tristique. Duis vitae turpis dapibus,
                                        facilisis magna ut, pretium metus.</p>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-xs-12">
                            <div class="tm-flex">
                                <div class="tm-bg-white-translucent text-xs-left tm-textbox tm-2-col-textbox-2 tm-textbox-padding">
                                    <h2 class="tm-text-title">Curabitur at sem</h2>
                                    <p class="tm-text">Curabitur ac bibendum augue, a convallis mi. Cum sociis natoque
                                        penatibus et magnis dis parturient montes, nascetur ridiculus mus. Sed ultrices
                                        placerat arcu.</p>
                                </div>
                                <div class="tm-bg-white-translucent text-xs-left tm-textbox tm-2-col-textbox-2 tm-textbox-padding">
                                    <h2 class="tm-text-title">Aliquam laoreet velit</h2>
                                    <p class="tm-text">Proin sagittis mauris dolor, vel efficitur lectus dictum nec. Sed
                                        ultrices placerat arcu, id malesuada metus cursus suscipit. Donex quis
                                        consectetur ligula. Thank you.</p>
                                </div>
                                <div class="tm-bg-white-translucent text-xs-left tm-textbox tm-2-col-textbox-2 tm-textbox-padding">
                                    <h2 class="tm-text-title">Suspendisse facilisis</h2>
                                    <p class="tm-text">Sed ultrices placerat arcu, id malesuada metus cursus suscipit.
                                        Donex quis consectetur ligula. Proin accumsan eros id nisi porttitor, a
                                        facilisis quam cursus.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div> <!-- .cd-full-width -->

        </li>

        <!-- Page 5 Contact Us -->
        <li>
            <div class="cd-full-width">
                <div class="container-fluid js-tm-page-content tm-page-pad" data-page-no="5">
                    <div class="tm-contact-page">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="tm-flex tm-contact-container">
                                    <div class="tm-bg-white-translucent text-xs-left tm-textbox tm-2-col-textbox-2 tm-textbox-padding tm-textbox-padding-contact">
                                        <h2 class="tm-contact-info">Say hello to us!</h2>
                                        <p class="tm-text">Pellentesque euismod, sem nec euismod interdum, odio elit
                                            venenatis est, gravida aliquet velit velit a ex. In luctus orci et orci
                                            lobortis, quis sagittis nibh laoreet.</p>

                                        <!-- contact form -->
                                        <form action="index.html" method="post" class="tm-contact-form">

                                            <div class="form-group">
                                                <input type="text" id="contact_name" name="contact_name"
                                                       class="form-control" placeholder="Name" required/>
                                            </div>

                                            <div class="form-group">
                                                <input type="email" id="contact_email" name="contact_email"
                                                       class="form-control" placeholder="Email" required/>
                                            </div>

                                            <div class="form-group">
                                                <textarea id="contact_message" name="contact_message"
                                                          class="form-control" rows="5" placeholder="Your message"
                                                          required></textarea>
                                            </div>

                                            <button type="submit" class="pull-xs-right tm-submit-btn">Send</button>

                                        </form>
                                    </div>

                                    <div class="tm-bg-white-translucent text-xs-left tm-textbox tm-2-col-textbox-2 tm-textbox-padding tm-textbox-padding-contact">
                                        <h2 class="tm-contact-info">794 Old Street 12120, San Francisco, CA</h2>
                                        <!-- google map goes here -->
                                        <div id="google-map"></div>
                                    </div>

                                </div>

                            </div>

                        </div>

                    </div>

                </div>

            </div> <!-- .cd-full-width -->
        </li>
    </ul> <!-- .cd-hero-slider -->

<?php
get_footer();
