<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package _s
 */

?>
<footer class="tm-footer">

    <div class="tm-social-icons-container text-xs-center">
        <a href="#" class="tm-social-link"><i class="fa fa-facebook"></i></a>
        <a href="#" class="tm-social-link"><i class="fa fa-google-plus"></i></a>
        <a href="#" class="tm-social-link"><i class="fa fa-twitter"></i></a>
        <a href="#" class="tm-social-link"><i class="fa fa-behance"></i></a>
        <a href="#" class="tm-social-link"><i class="fa fa-linkedin"></i></a>
    </div>

    <p class="tm-copyright-text">Copyright &copy; 2017 Your Company

        - Design: Tooplate</p>

</footer>

</div> <!-- .cd-hero -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
